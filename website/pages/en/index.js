/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require("react");
const classnames = require("classnames");

const CompLibrary = require("../../core/CompLibrary.js");

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const { siteConfig, language = "" } = this.props;
    const { baseUrl, docsUrl } = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ""}`;
    const langPart = `${language ? `${language}/` : ""}`;
    const docUrl = (doc) => `${baseUrl}${docsPart}${langPart}${doc}`;

    const SplashContainer = (props) => (
      <div className="homeContainer">
        <div className="homeSplashFade">
          <div className="wrapper homeWrapper">{props.children}</div>
        </div>
      </div>
    );

    const Logo = (props) => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const ProjectTitle = () => (
      <div>
        <h3 className="projectTitle mt-5">
          {siteConfig.title}
        </h3>
        <br></br>
        <img style={{height:"400px"}} src='/job-recommendation/img/dashboard.png' alt="Project Logo" /><br></br><br></br>
        <spam className="mt-4 mb-3"></spam><br></br>
        <spam className="mt-4 mb-3">This is an open source project developed under the GNU General Public License v3.0 <a href="https://choosealicense.com/licenses/gpl-3.0/">(learn more here)</a></spam>
        <br></br>
        
        <br></br>
        <br></br>
        <br></br>
        <Button className="d-inline" href="/job-recommendation/docs/introduction" target="_blank">
          Introduction
        </Button>
        <Button className="d-inline ml-3" href="/job-recommendation/docs/project_overview">
          Project Overview
        </Button>
      </div>
    );

    const PromoSection = (props) => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock">{props.children}</div>
        </div>
      </div>
    );

    const Button = (props) => {
      let classes = classnames(props.className, "pluginWrapper buttonWrapper");
      return (
        <div className={classes}>
          <a className="button" href={props.href} target={props.target}>
            {props.children}
          </a>
        </div>
      );
    };

    return (
      <SplashContainer>
        <div className="inner">
          <ProjectTitle siteConfig={siteConfig} />          
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const { config: siteConfig, language = "" } = this.props;
    const { baseUrl } = siteConfig;

    const Block = (props) => (
      <Container
        padding={["bottom", "top"]}
        id={props.id}
        background={props.background}
      >
        <GridBlock
          align="center"
          contents={props.children}
          layout={props.layout}
        />
      </Container>
    );

    const FeatureCallout = () => (
      <div
        className="productShowcaseSection"
        style={{ textAlign: "center" }}
      >
        <h3 className="projectTitle mt-5">
        Features
        </h3>
      
        <MarkdownBlock>
          {/* These are some ready to use functionalities of our project */}
        </MarkdownBlock>
      </div>
    );

    const TryOut = () => (
      <Block id="try">
        {[
          {
            content:
              "To make your landing page more attractive, use illustrations! Check out " +
              "[**unDraw**](https://undraw.co/) which provides you with customizable illustrations which are free to use. " +
              "The illustrations you see on this page are from unDraw.",
            image: `${baseUrl}img/undraw_code_review.svg`,
            imageAlign: "left",
            title: "Wonderful SVG Illustrations",
          },
        ]}
      </Block>
    );

    const Features = () => {
      return (
        <div>
          <Block background="light" className="testImg">
            {[
              {
                content:
                '<ul>'+
                  '<li>[TBC]</li>'+
                  '<li>[TBC]</li>'+
                '</ul>'
                  ,
                image: `${baseUrl}img/feature1.png`,
                imageAlign: "left",
                title: "",
              },
            ]}
          </Block>
          <Block background="dark">
            {[
              {
                content:
                '<ul>'+
                '<li>[TBC]</li>'+
              '</ul>'
                ,
                image: `${baseUrl}img/feature2.png`,
                imageAlign: "left",
                title: "",
              },
            ]}
          </Block>
          <Block background="light">
            {[
              {
                content:
                '<ul>'+
                '<li>[TBC]</li>'+
              '</ul>'
                ,
                image: `${baseUrl}img/feature3.png`,
                imageAlign: "left",
                title: "",
              },
            ]}
          </Block>
        </div>
      );
    };
    const Showcase = () => {
      if ((siteConfig.users || []).length === 0) {
        return null;
      }

      const showcase = siteConfig.users
        .filter((user) => user.pinned)
        .map((user) => (
          <a href={user.infoLink} key={user.infoLink}>
            <img src={user.image} alt={user.caption} title={user.caption} />
          </a>
        ));

      const pageUrl = (page) =>
        baseUrl + (language ? `${language}/` : "") + page;

      return (
        <div className="productShowcaseSection paddingBottom">
          <h2>Who is Using This?</h2>
          <p>This project is used by all these people</p>
          <div className="logos">{showcase}</div>
          <div className="more-users">
            <a className="button" href={pageUrl("users.html")}>
              More {siteConfig.title} Users
            </a>
          </div>
        </div>
      );
    };

    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer pt-0">
          {/* <FeatureCallout />
          <Features /> */}
          {/* <TryOut /> */}
          {/* <Showcase /> */}
        </div>
      </div>
    );
  }
}

module.exports = Index;
