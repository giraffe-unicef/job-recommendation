---
id: create_record 
title: Record Creation
sidebar_label: Record Creation
---

This section will provide a quick overview on how to use the Ontology admin dashboard manage records. 

(Record can be a Position, Qualification, Skill, Product and Service)

Topics covered:
- Creating a record
- Editing a record


<h3> Adding New Record </h3>

1. Access your Admin panel (http://localhost:3000)
2. Click on one of these -> Positions, Qualifications, Skills, Product and Services
    - This will open a view of all existing records added thus far
3. Click the "Add" button on the top right hand corner<br>
![Add](/job-recommendation/img/admin_user_guide/add.png)
4. Under "Name" - type the new record name
5. For Positions, You can also select if this position will have Agnostic Products And Services, Agnostic Qualifications, Agnostic Skills 
6. Click "Save" - this will add your new record to the list view


<h3> Editing existing records </h3>

1. To edit an existing record, first open the record view on the admin dashboard and find the relevant record (You can use search to find the relavant record)
2. Click "Edit"
3. Make the necessary changes
4. Click "Save"