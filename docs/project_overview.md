---
id: project_overview
title: Project Overview
sidebar_label: Project Overview
---

Project serves the purpose of creating a mapping between a certain position and associated skills, qualifications, products and services.


<h3>Admin panel</h3>

An administrator can create/edit positions, skills, qualifications, products and services. As well as creating a mapping for Positions to Skills, Positions to Qualifications, Positions to Products and Services. Admin can also manage mapping between an Position Category(Function) to Positions.

<h6> <b>CRUDs</b> </h6> 

This project consists of 4 different CRUDs :- Positions, Skills, Qualifications, Products and Services.

Skills, Qualifications, Products and Services can then be mapped out to certain Positions. 

Any mapping can be an agnostic or non agnostic. 

Agnostic mappings are not related to only certain industries, They are applied in general and Non Agnostic mappings are applied to specific industries. 

