---
id: contributing_guidelines
title: Contributing Guidelines
sidebar_label: Contributing Guidelines
---

Welcome to the contribution guidelines for Giraffe's open source Job's Ontology platform. This guide explains how to contribute to our project as well as define our working practices. This project is relatively new, as is this documentation - if you have any suggestions, please feel free to drop us a line at info@giraffe.co.za

<h3> Table of Contents </h3>
1. Asking Support Questions
2. Reporting Issues
3. Set up a Development Environment 
4. Feature Request (open a pull request)
5. Steps to submit a pull request
6. Labels/ Tags


<h3>1. Asking Support Questions</h3>

If you have any support question please email us directly at info@giraffe.co.za (use "Job's Ontology Support" in the subject)

<h3>2. Reporting Issues</h3>


If you believe you have found a defect or a bug on Giraffe's Job's Ontology or our documentation, use the GitLab issue tracker to report the problem to our maintainers. If you're not sure if your issue is a bug or not, you're welcome to start with a support question by emailing info@giraffe.co.za (use "Job's Ontology Support" in the subject). 

When reporting the issue, please provide all the relevant details including which aspect of the platform is affected, as well as the type of issue you're experiencing. We've added projects and tags to make this process easier.

**Submit issues here**: [https://gitlab.com/groups/giraffe-unicef/-/issues](https://gitlab.com/groups/giraffe-unicef/-/issues)

<h3>3. Set up a Development Environment </h3>

See here: [https://giraffe-unicef.gitlab.io/job-recommendation/docs/project_setup/](https://giraffe-unicef.gitlab.io/job-recommendation/docs/project_setup/)

<h3>4. Feature Request (open a pull request)</h3>

These guidelines help maintainers review new pull requests. Please stick to the guidelines for quicker and easier pull request reviews

Address the following questions in your pull request: <br>
1. What is a short summary of your change?
2. Why is this change helpful?
3. Any specific details to consider?
4. What is the desired outcome of your change?

<h3>5. Steps to submit a pull request</h3>

1. Find the project you want to contribute to
2. Fork it
3. Clone it to your local system
4. **Make** a new branch
5. **Make** your changes
6. Push it back to the repo
7. Click the Compare & **pull request** button
8. Click **Create pull request** to open a new **pull request**
9. Select the “feature request” template and fill it in
10. Submit your change!

<h3>6. Labels/ Tags</h3>

Below is a list of labels/ tags to easily classify your contribution. Where applicable, tag from this list when submitting your contribution


<table>
<tr>
<td>Bug</td>
<td>#eb2f06</td>
</tr>
<tr>
<td>Improvement </td>
<td>#6a89cc</td>
</tr>
<tr>
<td>Feature</td>
<td>#78e08f</td>
</tr>
<tr>
<td>Admin FrontEnd</td>
<td>#079992</td>
</tr>
<tr>
<td>User FrontEnd</td>
<td>#38ada9</td>
</tr>
<tr>
<td>BackEnd</td>
<td>#0a3d62</td>
</tr>
<tr>
<td>Critical</td>
<td>#b71540</td>
</tr>
<tr>
<td>Discussion</td>
<td>#fad390</td>
</tr>
<tr>
<td>Documentation</td>
<td>#fa983a</td>
</tr>
<tr>
<td>Support</td>
<td>#e58e26</td>
</tr>
<tr>
<td>Suggestion</td>
<td>#82ccdd</td>
</tr>
<tr>
<td>Regression</td>
<td>#60a3bc</td>
</tr>
</table>