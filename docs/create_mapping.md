---
id: create_mapping
title: Mapping Creation
sidebar_label: Mapping Creation
---

This section will provide a quick overview on how to use the Ontology admin dashboard to manage mappings. 

(Mapping can be a Position to Products and Services, Position To Qualifications, Position To Skills)

Topics covered:
- Creating a Mapping
- Editing a Mapping


<h3> Adding New Mapping </h3>

1. Access your Admin panel (http://localhost:3000)
2. Click on one of these -> Positions Mapping, Qualifications Mapping, Skills Mapping, Product and Services Mapping
    - This will open a view of all existing records added thus far
3. Click the "Add" button on the top right hand corner<br>
![Add](/job-recommendation/img/admin_user_guide/add.png)
4. Select the Position you want to create the mappings for.
5. Select if all these mappings are agnostic or not. 
6. If not agnostic, Select the industries you want to relate for this mapping
7. Select the Qualifications OR Skills OR Product and Services(The mapping to which you are creating)
6. Click "Save" - this will add your new record to the list view


<h3> Editing existing records </h3>

1. To edit an existing record, first open the record view on the admin dashboard and find the relevant record (You can use search to find the relavant record)
2. Click "Edit" 
3. Make the necessary changes (Here, You can select multiple Positions, They just have to have same agnostic property)
4. Click "Save"