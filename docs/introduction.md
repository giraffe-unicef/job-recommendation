---
id: introduction
title: Introduction
sidebar_label: Introduction
---


Giraffe is a South African mobile recruitment platform that focusses on addressing unemployment. Whilst South Africa’s unemployment problems relate primarily to the legacy of apartheid and country’s poor economic growth rate, access and visibility to opportunities are also major issues.
<br>

By using mobile technology and matching algorithms, Giraffe improves jobseekers’ access and visibility to opportunities. Since launching, Giraffe has attracted over a million jobseekers with thousands of businesses- both small and large- using the platform to recruit staff. 
<br>

However, one of the main lingering issues is the lack of education, skills and therefore employability- that plagues South Africa’s youth.
<br>

TBC