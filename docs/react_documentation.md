---
id: react_documentation
title: React documentation 
sidebar_label: React documentation 
---

 <h3>Positions Management</h3>


![Positions](/job-recommendation/img/ontology/positions.png)

* **Description**
    * All data here is static info except for the list of positions which is pulled dynamically
    * Agnostic represents that if this position will have mappings to some particular industries OR general.
* **APIs Used**
    * [Get positions](api_documentation#get-all-positions)
    * [Create position](api_documentation#create-positions)
    * [Update position](api_documentation#update-positions)
* **Actions**
    1. Add a new position.
    2. Edit existing positions.


<h3>Products and Services Management</h3>

![Products and Services](/job-recommendation/img/ontology/products-and-services.png)

* **Description**
    * All data here is static info except for the list of products and services which is pulled dynamically
* **APIs Used**
    * [Get Products and Services](api_documentation#get-all-products-and-services)
    * [Create Products and Services](api_documentation#create-products-and-services)
    * [Update Products and Services](api_documentation#update-products-and-services)
* **Actions**
    1. Add a new product and service.
    2. Edit existing products and services.


<h3>Qualifications Management</h3>

![Qualifications](/job-recommendation/img/ontology/qualifications.png)

* **Description**
    * All data here is static info except for the list of qualifications which is pulled dynamically
* **APIs Used**
    * [Get Qualifications](api_documentation#get-all-qualifications)
    * [Create Qualifications](api_documentation#create-qualifications)
    * [Update Qualifications](api_documentation#update-qualifications)
* **Actions**
    1. Add a new qualification.
    2. Edit existing qualifications.


<h3>Skills Management</h3>

![Skills](/job-recommendation/img/ontology/skills.png)

* **Description**
    * All data here is static info except for the list of skills which is pulled dynamically
* **APIs Used**
    * [Get Skills](api_documentation#get-all-skills)
    * [Create Skills](api_documentation#create-skills)
    * [Update Skills](api_documentation#update-skills)
* **Actions**
    1. Add a new skill.
    2. Edit existing skill.


<h3>Products and Services Mapping</h3>

![Products and Services Mapping](/job-recommendation/img/ontology/products-and-services-mapping.png)

* **Description**
    * All data here is static info except for the list of mappings for positions to products and services which is pulled dynamically
    * There are 2 types of mappings, Agnostic and not Agnostic, An Agnostic mapping as the name suggests is a general mapping between position and products and services While A not Agnostic mapping belongs to only certain industries.
* **APIs Used**
    * [Get positions](api_documentation#get-all-positions)
    * [Get industries](api_documentation#get-all-industries)
    * [Get Products and Services](api_documentation#get-all-products-and-services)
    * [Get Products and Services Mapping](api_documentation#get-all-industry-position-products-and-services-group-mapping)
    * [Create Products and Services Mapping](api_documentation#create-industry-position-products-and-services-group-mapping)
    * [Update Products and Services Mapping](api_documentation#update-industry-position-products-and-services-group-mapping)
* **Actions**
    1. Add a new mapping for position to products and services.
    2. Edit an existing mapping for position to products and services.


<h3>Qualifications Mapping</h3>

![Qualifications Mapping](/job-recommendation/img/ontology/qualifications-mapping.png)

* **Description**
    * All data here is static info except for the list of mappings for positions to qualifications which is pulled dynamically
    * There are 2 types of mappings, Agnostic and not Agnostic, An Agnostic mapping as the name suggests is a general mapping between position and qualifications While A not Agnostic mapping belongs to only certain industries.
* **APIs Used**
    * [Get positions](api_documentation#get-all-positions)
    * [Get industries](api_documentation#get-all-industries)
    * [Get Qualifications](api_documentation#get-all-qualifications)
    * [Get Qualifications Mapping](api_documentation#get-all-industry-position-products-and-services-group-mapping)
    * [Create Qualifications Mapping](api_documentation#create-industry-position-products-and-services-group-mapping)
    * [Update Qualifications Mapping](api_documentation#update-industry-position-products-and-services-group-mapping)
* **Actions**
    1. Add a new mapping for position to qualifications.
    2. Edit an existing mapping for position to qualifications.


<h3>Skills Mapping</h3>

![Skills Mapping](/job-recommendation/img/ontology/skills-mapping.png)

* **Description**
    * All data here is static info except for the list of mappings for positions to Skills which is pulled dynamically
    * There are 2 types of mappings, Agnostic and not Agnostic, An Agnostic mapping as the name suggests is a general mapping between position and Skills While A not Agnostic mapping belongs to only certain industries.
* **APIs Used**
    * [Get positions](api_documentation#get-all-positions)
    * [Get industries](api_documentation#get-all-industries)
    * [Get Skills](api_documentation#get-all-skills)
    * [Get Skills Mapping](api_documentation#get-all-industry-position-skills-group-mapping)
    * [Create Skills Mapping](api_documentation#create-industry-position-skills-group-mapping)
    * [Update Skills Mapping](api_documentation#update-industry-position-skills-group-mapping)
* **Actions**
    1. Add a new mapping for position to Skills.
    2. Edit an existing mapping for position to Skills.

<h3>Positions Mapping</h3>

![Positions Mapping](/job-recommendation/img/ontology/positions-mapping.png)

* **Description**
    * All data here is static info except for the list of mappings for position categories (functions) to positions which is pulled dynamically    
* **APIs Used**
    * [Get positions](api_documentation#get-all-positions)
    * [Get functions](api_documentation#get-all-functions)
    * [Get Functions with Positions](api_documentation#get-all-positions-with-categories)    
    * [Create OR update Functions with Positions mapping Mapping](api_documentation#create-positions-categories-mapping)
* **Actions**
    1. Add a new mapping for position categories (functions) to positions.
    2. Edit an existing mapping for position categories (functions) to positions.