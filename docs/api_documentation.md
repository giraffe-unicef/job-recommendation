---
id: api_documentation
title: API Documentation
sidebar_label: API Documentation
---
Base URLs:

* <a href="http://localhost:8080/v1">http://localhost:8080/v1</a>

Email: <a href="mailto:shafin@giraffe.co.za">Support</a> 

## **Auth API**
<!-- <h1 id="giraffe-opensource-ontlogy-editor-apis-auth-api">Auth API</h1> -->

Auth Api Controller

### Login user

<a id="opIdloginUser"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/login");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/login HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: application/json

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "password": "string",
  "username": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/login',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /login`

Login user for ops tool

> Body parameter

```json
{
  "password": "string",
  "username": "string"
}
```

<h3 id="login-user-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[AuthRequest](#schemaauthrequest)|false|authRequest|

> Example responses

> 200 Response

```json
[
  true
]
```

<h3 id="login-user-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|bad request|string|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|Not Found|None|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="login-user-responseschema">Response Schema</h3>

<aside class="success">
This operation does not require authentication
</aside>


## **Ontology API**
<!-- <h1 id="giraffe-opensource-ontlogy-editor-apis-ontology-api">Ontology API</h1> -->

 

#### Get all functions

<a id="opIdgetAllFunctions"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/functions");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/functions HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/functions',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /functions`

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get-all-functions-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-functions-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[Function](#schemafunction)]|false|none|none|
|» Function|[Function](#schemafunction)|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» name|string|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Get all industries

<a id="opIdgetAllIndustries"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/industries");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/industries HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/industries',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /industries`

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get-all-industries-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-industries-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[Industry](#schemaindustry)]|false|none|none|
|» Industry|[Industry](#schemaindustry)|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» name|string|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Get positions by function ID

<a id="opIdgetPositionsByFunctionIds"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/positions?function_id=0");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/positions?function_id=0 HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/positions?function_id=0',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /positions`

<h3 id="get-positions-by-function-id-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|function_id|query|integer(int32)|true|function_id|

> Example responses

> 200 Response

```json
[
  {
    "description": "string",
    "id": 0
  }
]
```

<h3 id="get-positions-by-function-id-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-positions-by-function-id-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[Position](#schemaposition)]|false|none|none|
|» Position|[Position](#schemaposition)|false|none|none|
|»» description|string|false|none|none|
|»» id|integer(int32)|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Get products and services by industry ID and position IDs

<a id="opIdgetProductsAndServicesByIndustryAndPositionIds"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/products-and-services");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/products-and-services HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/products-and-services',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /products-and-services`

<h3 id="get-products-and-services-by-industry-id-and-position-ids-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|industry_id|query|integer(int32)|false|Industry ID|
|position_ids|query|array[integer]|false|One or more position IDs|

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get-products-and-services-by-industry-id-and-position-ids-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-products-and-services-by-industry-id-and-position-ids-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[ProductsAndServices](#schemaproductsandservices)]|false|none|none|
|» ProductsAndServices|[ProductsAndServices](#schemaproductsandservices)|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» name|string|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Get qualifications by industry ID and position IDs

<a id="opIdgetQualificationsByIndustryAndPositionIds"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/qualifications");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/qualifications HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/qualifications',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /qualifications`

<h3 id="get-qualifications-by-industry-id-and-position-ids-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|industry_id|query|integer(int32)|false|Industry ID|
|position_ids|query|array[integer]|false|One or more position IDs|

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get-qualifications-by-industry-id-and-position-ids-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-qualifications-by-industry-id-and-position-ids-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[Qualification](#schemaqualification)]|false|none|none|
|» Qualification|[Qualification](#schemaqualification)|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» name|string|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Get skills by industry ID and position IDs

<a id="opIdgetSkillsByIndustryAndPositionIds"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/skills");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/skills HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/skills',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /skills`

<h3 id="get-skills-by-industry-id-and-position-ids-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|industry_id|query|integer(int32)|false|Industry ID|
|position_ids|query|array[integer]|false|One or more position IDs|

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get-skills-by-industry-id-and-position-ids-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-skills-by-industry-id-and-position-ids-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[Skill](#schemaskill)]|false|none|none|
|» Skill|[Skill](#schemaskill)|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» name|string|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

## **Ontology Editor API**
<!-- <h1 id="giraffe-opensource-ontlogy-editor-apis-ontology-editor-api">Ontology Editor API</h1> -->

Ontology Editor Api Controller

### Get all positions

<a id="opIdgetALlPositions"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/positions");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/ontology/positions HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/positions',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /ontology/positions`

<h3 id="get-all-positions-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|agnostic_industry|query|boolean|false|agnostic_industry|
|section|query|string|false|section|

> Example responses

> 200 Response

```json
[
  {
    "agnostic_products_and_services": true,
    "agnostic_qualification": true,
    "agnostic_skill": true,
    "description": "string",
    "id": 0
  }
]
```

<h3 id="get-all-positions-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-positions-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[PositionWithAgnosticMapping](#schemapositionwithagnosticmapping)]|false|none|none|
|» PositionWithAgnosticMapping|[PositionWithAgnosticMapping](#schemapositionwithagnosticmapping)|false|none|none|
|»» agnostic_products_and_services|boolean|false|none|none|
|»» agnostic_qualification|boolean|false|none|none|
|»» agnostic_skill|boolean|false|none|none|
|»» description|string|false|none|none|
|»» id|integer(int32)|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Create positions

<a id="opIdcreatePositions"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/positions");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/ontology/positions HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: application/json

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "agnostic_products_and_services": true,
  "agnostic_qualification": true,
  "agnostic_skill": true,
  "name": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/positions',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /ontology/positions`

> Body parameter

```json
{
  "agnostic_products_and_services": true,
  "agnostic_qualification": true,
  "agnostic_skill": true,
  "name": "string"
}
```

<h3 id="create-positions-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[PositionRequest](#schemapositionrequest)|false|body|

> Example responses

> 200 Response

```json
{
  "agnostic_products_and_services": true,
  "agnostic_qualification": true,
  "agnostic_skill": true,
  "description": "string",
  "id": 0
}
```

<h3 id="create-positions-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|[PositionWithAgnosticMapping](#schemapositionwithagnosticmapping)|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get all positions with categories

<a id="opIdgetALlPositionsFunctionMapping"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/positions-categories");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/ontology/positions-categories HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/positions-categories',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /ontology/positions-categories`

> Example responses

> 200 Response

```json
[
  {
    "description": "string",
    "id": 0,
    "position": [
      {
        "description": "string",
        "id": 0
      }
    ]
  }
]
```

<h3 id="get-all-positions-with-categories-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-positions-with-categories-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[PositionFunctionMapping](#schemapositionfunctionmapping)]|false|none|none|
|» PositionFunctionMapping|[PositionFunctionMapping](#schemapositionfunctionmapping)|false|none|none|
|»» description|string|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» position|[[Position](#schemaposition)]|false|none|none|
|»»» Position|[Position](#schemaposition)|false|none|none|
|»»»» description|string|false|none|none|
|»»»» id|integer(int32)|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Create positions categories mapping

<a id="opIdcreatePositionsFunctionMapping"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/positions-categories");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/ontology/positions-categories HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "deleted_position_ids": [
    0
  ],
  "function_id": 0,
  "position_ids": [
    0
  ]
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/positions-categories',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /ontology/positions-categories`

> Body parameter

```json
{
  "deleted_position_ids": [
    0
  ],
  "function_id": 0,
  "position_ids": [
    0
  ]
}
```

<h3 id="create-positions-categories-mapping-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[UpsertPositionFunctionMapping](#schemaupsertpositionfunctionmapping)|false|upsertPositionFunctionMapping|

> Example responses

> 200 Response

<h3 id="create-positions-categories-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update positions

<a id="opIdupdatePositions"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/positions/{id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8080/v1/ontology/positions/{id} HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "name": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/positions/{id}',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PUT /ontology/positions/{id}`

> Body parameter

```json
{
  "name": "string"
}
```

<h3 id="update-positions-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int32)|true|Position ID|
|body|body|[OntologyEditorRequest](#schemaontologyeditorrequest)|false|ontologyEditorRequest|

> Example responses

> 200 Response

<h3 id="update-positions-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get all products and services

<a id="opIdgetAllProductsAndServices"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/products-and-services");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/ontology/products-and-services HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/products-and-services',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /ontology/products-and-services`

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get-all-products-and-services-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-products-and-services-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[ProductsAndServices](#schemaproductsandservices)]|false|none|none|
|» ProductsAndServices|[ProductsAndServices](#schemaproductsandservices)|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» name|string|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Create products and services

<a id="opIdcreateProductsAndServices"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/products-and-services");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/ontology/products-and-services HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "name": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/products-and-services',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /ontology/products-and-services`

> Body parameter

```json
{
  "name": "string"
}
```

<h3 id="create-products-and-services-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[OntologyEditorRequest](#schemaontologyeditorrequest)|false|ontologyEditorRequest|

> Example responses

> 200 Response

<h3 id="create-products-and-services-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update products and services

<a id="opIdupdateProductsAndServices"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/products-and-services/{id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8080/v1/ontology/products-and-services/{id} HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "name": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/products-and-services/{id}',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->


`PUT /ontology/products-and-services/{id}`

> Body parameter

```json
{
  "name": "string"
}
```

<h3 id="update-products-and-services-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int32)|true|Products and services ID|
|body|body|[OntologyEditorRequest](#schemaontologyeditorrequest)|false|ontologyEditorRequest|

> Example responses

> 200 Response

<h3 id="update-products-and-services-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get all Industry Position Products and Services Group Mapping

<a id="opIdgetIndustryPositionProductsServicesGroup"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/products-services-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/ontology/products-services-group-mapping HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/products-services-group-mapping',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /ontology/products-services-group-mapping`

> Example responses

> 200 Response

```json
[
  {
    "agnosticProductsServices": true,
    "industryIds": [
      0
    ],
    "positionIds": [
      0
    ],
    "productServiceIds": [
      0
    ]
  }
]
```

<h3 id="get-all-industry-position-products-and-services-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-industry-position-products-and-services-group-mapping-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[IndustryPositionProductsServicesGroup](#schemaindustrypositionproductsservicesgroup)]|false|none|none|
|» IndustryPositionProductsServicesGroup|[IndustryPositionProductsServicesGroup](#schemaindustrypositionproductsservicesgroup)|false|none|none|
|»» agnosticProductsServices|boolean|false|none|none|
|»» industryIds|[integer]|false|none|none|
|»» positionIds|[integer]|false|none|none|
|»» productServiceIds|[integer]|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Create Industry Position Products and Services Group Mapping

<a id="opIdcreateIndustryPositionProductAndServiceGroupMapping"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/products-services-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/ontology/products-services-group-mapping HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "agnostic_industry": true,
  "industry_ids": [
    0
  ],
  "position_id": 0,
  "section_ids": [
    0
  ]
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/products-services-group-mapping',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /ontology/products-services-group-mapping`

> Body parameter

```json
{
  "agnostic_industry": true,
  "industry_ids": [
    0
  ],
  "position_id": 0,
  "section_ids": [
    0
  ]
}
```

<h3 id="create-industry-position-products-and-services-group-mapping-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[CreateOntologyMapping](#schemacreateontologymapping)|false|createOntologyMapping|

> Example responses

> 200 Response

<h3 id="create-industry-position-products-and-services-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update Industry Position Products and Services Group Mapping

<a id="opIdupdateIndustryPositionProductAndServiceGroupMapping"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/products-services-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8080/v1/ontology/products-services-group-mapping HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "agnostic_industry": true,
  "deleted_industry_ids": [
    0
  ],
  "deleted_position_ids": [
    0
  ],
  "deleted_section_ids": [
    0
  ],
  "industry_id": [
    0
  ],
  "position_id": [
    0
  ],
  "section_ids": [
    0
  ]
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/products-services-group-mapping',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PUT /ontology/products-services-group-mapping`

> Body parameter

```json
{
  "agnostic_industry": true,
  "deleted_industry_ids": [
    0
  ],
  "deleted_position_ids": [
    0
  ],
  "deleted_section_ids": [
    0
  ],
  "industry_id": [
    0
  ],
  "position_id": [
    0
  ],
  "section_ids": [
    0
  ]
}
```

<h3 id="update-industry-position-products-and-services-group-mapping-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[UpdateOntologyMapping](#schemaupdateontologymapping)|false|updateOntologyMapping|

> Example responses

> 200 Response

<h3 id="update-industry-position-products-and-services-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get all qualifications

<a id="opIdgetAllQualifications"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/qualifications");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/ontology/qualifications HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/qualifications',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /ontology/qualifications`

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get-all-qualifications-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-qualifications-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[Qualification](#schemaqualification)]|false|none|none|
|» Qualification|[Qualification](#schemaqualification)|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» name|string|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Create qualifications

<a id="opIdcreateQualifications"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/qualifications");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/ontology/qualifications HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "name": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/qualifications',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /ontology/qualifications`

> Body parameter

```json
{
  "name": "string"
}
```

<h3 id="create-qualifications-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[OntologyEditorRequest](#schemaontologyeditorrequest)|false|ontologyEditorRequest|

> Example responses

> 200 Response

<h3 id="create-qualifications-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get all Industry Position Qualifications Group Mapping

<a id="opIdgetIndustryPositionQualificationsGroup"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/qualifications-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/ontology/qualifications-group-mapping HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/qualifications-group-mapping',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /ontology/qualifications-group-mapping`

> Example responses

> 200 Response

```json
[
  {
    "agnosticQualification": true,
    "industryIds": [
      0
    ],
    "positionIds": [
      0
    ],
    "qualificationIds": [
      0
    ]
  }
]
```

<h3 id="get-all-industry-position-qualifications-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-industry-position-qualifications-group-mapping-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[IndustryPositionQualificationsGroup](#schemaindustrypositionqualificationsgroup)]|false|none|none|
|» IndustryPositionQualificationsGroup|[IndustryPositionQualificationsGroup](#schemaindustrypositionqualificationsgroup)|false|none|none|
|»» agnosticQualification|boolean|false|none|none|
|»» industryIds|[integer]|false|none|none|
|»» positionIds|[integer]|false|none|none|
|»» qualificationIds|[integer]|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Create Industry Position Qualification Group Mapping

<a id="opIdcreateIndustryPositionQualificationsGroupMapping"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/qualifications-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/ontology/qualifications-group-mapping HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "agnostic_industry": true,
  "industry_ids": [
    0
  ],
  "position_id": 0,
  "section_ids": [
    0
  ]
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/qualifications-group-mapping',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /ontology/qualifications-group-mapping`

> Body parameter

```json
{
  "agnostic_industry": true,
  "industry_ids": [
    0
  ],
  "position_id": 0,
  "section_ids": [
    0
  ]
}
```

<h3 id="create-industry-position-qualification-group-mapping-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[CreateOntologyMapping](#schemacreateontologymapping)|false|createOntologyMapping|

> Example responses

> 200 Response

<h3 id="create-industry-position-qualification-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update Industry Position Qualification Group Mapping

<a id="opIdupdateIndustryPositionQualificationsGroupMapping"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/qualifications-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8080/v1/ontology/qualifications-group-mapping HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "agnostic_industry": true,
  "deleted_industry_ids": [
    0
  ],
  "deleted_position_ids": [
    0
  ],
  "deleted_section_ids": [
    0
  ],
  "industry_id": [
    0
  ],
  "position_id": [
    0
  ],
  "section_ids": [
    0
  ]
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/qualifications-group-mapping',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PUT /ontology/qualifications-group-mapping`

> Body parameter

```json
{
  "agnostic_industry": true,
  "deleted_industry_ids": [
    0
  ],
  "deleted_position_ids": [
    0
  ],
  "deleted_section_ids": [
    0
  ],
  "industry_id": [
    0
  ],
  "position_id": [
    0
  ],
  "section_ids": [
    0
  ]
}
```

<h3 id="update-industry-position-qualification-group-mapping-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[UpdateOntologyMapping](#schemaupdateontologymapping)|false|updateOntologyMapping|

> Example responses

> 200 Response

<h3 id="update-industry-position-qualification-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update qualifications

<a id="opIdupdateQualifications"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/qualifications/{id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8080/v1/ontology/qualifications/{id} HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "name": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/qualifications/{id}',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PUT /ontology/qualifications/{id}`

> Body parameter

```json
{
  "name": "string"
}
```

<h3 id="update-qualifications-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int32)|true|Qualifications ID|
|body|body|[OntologyEditorRequest](#schemaontologyeditorrequest)|false|ontologyEditorRequest|

> Example responses

> 200 Response

<h3 id="update-qualifications-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get all skills

<a id="opIdgetAllSkills"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/skills");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/ontology/skills HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/skills',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /ontology/skills`

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get-all-skills-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-skills-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[Skill](#schemaskill)]|false|none|none|
|» Skill|[Skill](#schemaskill)|false|none|none|
|»» id|integer(int32)|false|none|none|
|»» name|string|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Create skills

<a id="opIdcreateSkills"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/skills");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/ontology/skills HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "name": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/skills',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /ontology/skills`

> Body parameter

```json
{
  "name": "string"
}
```

<h3 id="create-skills-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[OntologyEditorRequest](#schemaontologyeditorrequest)|false|ontologyEditorRequest|

> Example responses

> 200 Response

<h3 id="create-skills-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Get all Industry Position Skills Group Mapping

<a id="opIdgetIndustryPositionSkillsGroup"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/skills-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("GET");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
GET http://localhost:8080/v1/ontology/skills-group-mapping HTTP/1.1
Host: localhost:8080
Accept: application/json

```
<!--JavaScript-->
```javascript

const headers = {
  'Accept':'application/json'
};

fetch('http://localhost:8080/v1/ontology/skills-group-mapping',
{
  method: 'GET',

  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`GET /ontology/skills-group-mapping`

> Example responses

> 200 Response

```json
[
  {
    "agnosticSkill": true,
    "industryIds": [
      0
    ],
    "positionIds": [
      0
    ],
    "skillIds": [
      0
    ]
  }
]
```

<h3 id="get-all-industry-position-skills-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<h3 id="get-all-industry-position-skills-group-mapping-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[IndustryPositionSkillsGroup](#schemaindustrypositionskillsgroup)]|false|none|none|
|» IndustryPositionSkillsGroup|[IndustryPositionSkillsGroup](#schemaindustrypositionskillsgroup)|false|none|none|
|»» agnosticSkill|boolean|false|none|none|
|»» industryIds|[integer]|false|none|none|
|»» positionIds|[integer]|false|none|none|
|»» skillIds|[integer]|false|none|none|

<aside class="success">
This operation does not require authentication
</aside>

### Create Industry Position Skills Group Mapping

<a id="opIdcreateIndustryPositionSkillsGroupMapping"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/skills-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("POST");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
POST http://localhost:8080/v1/ontology/skills-group-mapping HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "agnostic_industry": true,
  "industry_ids": [
    0
  ],
  "position_id": 0,
  "section_ids": [
    0
  ]
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/skills-group-mapping',
{
  method: 'POST',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`POST /ontology/skills-group-mapping`

> Body parameter

```json
{
  "agnostic_industry": true,
  "industry_ids": [
    0
  ],
  "position_id": 0,
  "section_ids": [
    0
  ]
}
```

<h3 id="create-industry-position-skills-group-mapping-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[CreateOntologyMapping](#schemacreateontologymapping)|false|createOntologyMapping|

> Example responses

> 200 Response

<h3 id="create-industry-position-skills-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update Industry Position Skills Group Mapping

<a id="opIdupdateIndustryPositionSkillsGroupMapping"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/skills-group-mapping");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8080/v1/ontology/skills-group-mapping HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "agnostic_industry": true,
  "deleted_industry_ids": [
    0
  ],
  "deleted_position_ids": [
    0
  ],
  "deleted_section_ids": [
    0
  ],
  "industry_id": [
    0
  ],
  "position_id": [
    0
  ],
  "section_ids": [
    0
  ]
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/skills-group-mapping',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PUT /ontology/skills-group-mapping`

> Body parameter

```json
{
  "agnostic_industry": true,
  "deleted_industry_ids": [
    0
  ],
  "deleted_position_ids": [
    0
  ],
  "deleted_section_ids": [
    0
  ],
  "industry_id": [
    0
  ],
  "position_id": [
    0
  ],
  "section_ids": [
    0
  ]
}
```

<h3 id="update-industry-position-skills-group-mapping-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[UpdateOntologyMapping](#schemaupdateontologymapping)|false|updateOntologyMapping|

> Example responses

> 200 Response

<h3 id="update-industry-position-skills-group-mapping-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

### Update skills

<a id="opIdupdateSkills"></a>

> Code samples

<!--DOCUSAURUS_CODE_TABS-->

<!--Java-->
```java
URL obj = new URL("http://localhost:8080/v1/ontology/skills/{id}");
HttpURLConnection con = (HttpURLConnection) obj.openConnection();
con.setRequestMethod("PUT");
int responseCode = con.getResponseCode();
BufferedReader in = new BufferedReader(
    new InputStreamReader(con.getInputStream()));
String inputLine;
StringBuffer response = new StringBuffer();
while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
}
in.close();
System.out.println(response.toString());

```
<!--HTTP-->
```http
PUT http://localhost:8080/v1/ontology/skills/{id} HTTP/1.1
Host: localhost:8080
Content-Type: application/json
Accept: */*

```
<!--JavaScript-->
```javascript
const inputBody = '{
  "name": "string"
}';
const headers = {
  'Content-Type':'application/json',
  'Accept':'*/*'
};

fetch('http://localhost:8080/v1/ontology/skills/{id}',
{
  method: 'PUT',
  body: inputBody,
  headers: headers
})
.then(function(res) {
    return res.json();
}).then(function(body) {
    console.log(body);
});

```
<!--END_DOCUSAURUS_CODE_TABS-->

`PUT /ontology/skills/{id}`

> Body parameter

```json
{
  "name": "string"
}
```

<h3 id="update-skills-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|id|path|integer(int32)|true|Skills ID|
|body|body|[OntologyEditorRequest](#schemaontologyeditorrequest)|false|ontologyEditorRequest|

> Example responses

> 200 Response

<h3 id="update-skills-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|successful operation|string|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|None|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Unauthorized|None|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|None|
|404|[Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4)|resource not found|string|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|internal server error|string|

<aside class="success">
This operation does not require authentication
</aside>

## **Schemas**

### AuthRequest
<a id="schemaauthrequest"></a>
<a id="schema_AuthRequest"></a>
<a id="tocSauthrequest"></a>
<a id="tocsauthrequest"></a>

```json
{
  "password": "string",
  "username": "string"
}

```

#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|password|string|false|none|none|
|username|string|false|none|none|


### AuthResponse
<a id="schemaauthresponse"></a>
<a id="schema_AuthResponse"></a>
<a id="tocSauthresponse"></a>
<a id="tocsauthresponse"></a>

```json
{
  "is_admin": true,
  "verified": true
}

```

#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|is_admin|boolean|false|none|none|
|verified|boolean|false|none|none|


### CreateOntologyMapping

<a id="schemacreateontologymapping"></a>
<a id="schema_CreateOntologyMapping"></a>
<a id="tocScreateontologymapping"></a>
<a id="tocscreateontologymapping"></a>

```json
{
  "agnostic_industry": true,
  "industry_ids": [
    0
  ],
  "position_id": 0,
  "section_ids": [
    0
  ]
}

```


#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|agnostic_industry|boolean|false|none|none|
|industry_ids|[integer]|false|none|none|
|position_id|integer(int32)|false|none|none|
|section_ids|[integer]|false|none|none|

### Function

<a id="schemafunction"></a>
<a id="schema_Function"></a>
<a id="tocSfunction"></a>
<a id="tocsfunction"></a>

```json
{
  "id": 0,
  "name": "string"
}

```


#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|

### Industry

<a id="schemaindustry"></a>
<a id="schema_Industry"></a>
<a id="tocSindustry"></a>
<a id="tocsindustry"></a>

```json
{
  "id": 0,
  "name": "string"
}

```


#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|

### IndustryPositionProductsServicesGroup

<a id="schemaindustrypositionproductsservicesgroup"></a>
<a id="schema_IndustryPositionProductsServicesGroup"></a>
<a id="tocSindustrypositionproductsservicesgroup"></a>
<a id="tocsindustrypositionproductsservicesgroup"></a>

```json
{
  "agnosticProductsServices": true,
  "industryIds": [
    0
  ],
  "positionIds": [
    0
  ],
  "productServiceIds": [
    0
  ]
}

```

#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|agnosticProductsServices|boolean|false|none|none|
|industryIds|[integer]|false|none|none|
|positionIds|[integer]|false|none|none|
|productServiceIds|[integer]|false|none|none|

### IndustryPositionQualificationsGroup

<a id="schemaindustrypositionqualificationsgroup"></a>
<a id="schema_IndustryPositionQualificationsGroup"></a>
<a id="tocSindustrypositionqualificationsgroup"></a>
<a id="tocsindustrypositionqualificationsgroup"></a>

```json
{
  "agnosticQualification": true,
  "industryIds": [
    0
  ],
  "positionIds": [
    0
  ],
  "qualificationIds": [
    0
  ]
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|agnosticQualification|boolean|false|none|none|
|industryIds|[integer]|false|none|none|
|positionIds|[integer]|false|none|none|
|qualificationIds|[integer]|false|none|none|

### IndustryPositionSkillsGroup

<a id="schemaindustrypositionskillsgroup"></a>
<a id="schema_IndustryPositionSkillsGroup"></a>
<a id="tocSindustrypositionskillsgroup"></a>
<a id="tocsindustrypositionskillsgroup"></a>

```json
{
  "agnosticSkill": true,
  "industryIds": [
    0
  ],
  "positionIds": [
    0
  ],
  "skillIds": [
    0
  ]
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|agnosticSkill|boolean|false|none|none|
|industryIds|[integer]|false|none|none|
|positionIds|[integer]|false|none|none|
|skillIds|[integer]|false|none|none|

### OntologyEditorRequest

<a id="schemaontologyeditorrequest"></a>
<a id="schema_OntologyEditorRequest"></a>
<a id="tocSontologyeditorrequest"></a>
<a id="tocsontologyeditorrequest"></a>

```json
{
  "name": "string"
}

```


#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|name|string|false|none|none|

### Position

<a id="schemaposition"></a>
<a id="schema_Position"></a>
<a id="tocSposition"></a>
<a id="tocsposition"></a>

```json
{
  "description": "string",
  "id": 0
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|description|string|false|none|none|
|id|integer(int32)|false|none|none|

### PositionFunctionMapping

<a id="schemapositionfunctionmapping"></a>
<a id="schema_PositionFunctionMapping"></a>
<a id="tocSpositionfunctionmapping"></a>
<a id="tocspositionfunctionmapping"></a>

```json
{
  "description": "string",
  "id": 0,
  "position": [
    {
      "description": "string",
      "id": 0
    }
  ]
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|description|string|false|none|none|
|id|integer(int32)|false|none|none|
|position|[[Position](#schemaposition)]|false|none|none|

### PositionRequest

<a id="schemapositionrequest"></a>
<a id="schema_PositionRequest"></a>
<a id="tocSpositionrequest"></a>
<a id="tocspositionrequest"></a>

```json
{
  "agnostic_products_and_services": true,
  "agnostic_qualification": true,
  "agnostic_skill": true,
  "name": "string"
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|agnostic_products_and_services|boolean|false|none|none|
|agnostic_qualification|boolean|false|none|none|
|agnostic_skill|boolean|false|none|none|
|name|string|false|none|none|

### PositionWithAgnosticMapping

<a id="schemapositionwithagnosticmapping"></a>
<a id="schema_PositionWithAgnosticMapping"></a>
<a id="tocSpositionwithagnosticmapping"></a>
<a id="tocspositionwithagnosticmapping"></a>

```json
{
  "agnostic_products_and_services": true,
  "agnostic_qualification": true,
  "agnostic_skill": true,
  "description": "string",
  "id": 0
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|agnostic_products_and_services|boolean|false|none|none|
|agnostic_qualification|boolean|false|none|none|
|agnostic_skill|boolean|false|none|none|
|description|string|false|none|none|
|id|integer(int32)|false|none|none|

### ProductsAndServices

<a id="schemaproductsandservices"></a>
<a id="schema_ProductsAndServices"></a>
<a id="tocSproductsandservices"></a>
<a id="tocsproductsandservices"></a>

```json
{
  "id": 0,
  "name": "string"
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|

### Qualification

<a id="schemaqualification"></a>
<a id="schema_Qualification"></a>
<a id="tocSqualification"></a>
<a id="tocsqualification"></a>

```json
{
  "id": 0,
  "name": "string"
}

```


#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|

### Skill

<a id="schemaskill"></a>
<a id="schema_Skill"></a>
<a id="tocSskill"></a>
<a id="tocsskill"></a>

```json
{
  "id": 0,
  "name": "string"
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|none|
|name|string|false|none|none|

### UpdateOntologyMapping

<a id="schemaupdateontologymapping"></a>
<a id="schema_UpdateOntologyMapping"></a>
<a id="tocSupdateontologymapping"></a>
<a id="tocsupdateontologymapping"></a>

```json
{
  "agnostic_industry": true,
  "deleted_industry_ids": [
    0
  ],
  "deleted_position_ids": [
    0
  ],
  "deleted_section_ids": [
    0
  ],
  "industry_id": [
    0
  ],
  "position_id": [
    0
  ],
  "section_ids": [
    0
  ]
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|agnostic_industry|boolean|false|none|none|
|deleted_industry_ids|[integer]|false|none|none|
|deleted_position_ids|[integer]|false|none|none|
|deleted_section_ids|[integer]|false|none|none|
|industry_id|[integer]|false|none|none|
|position_id|[integer]|false|none|none|
|section_ids|[integer]|false|none|none|

### UpsertPositionFunctionMapping

<a id="schemaupsertpositionfunctionmapping"></a>
<a id="schema_UpsertPositionFunctionMapping"></a>
<a id="tocSupsertpositionfunctionmapping"></a>
<a id="tocsupsertpositionfunctionmapping"></a>

```json
{
  "deleted_position_ids": [
    0
  ],
  "function_id": 0,
  "position_ids": [
    0
  ]
}

```



#### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|deleted_position_ids|[integer]|false|none|none|
|function_id|integer(int32)|false|none|none|
|position_ids|[integer]|false|none|none|