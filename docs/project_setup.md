---
id: project_setup
title: Project Setup
sidebar_label: Project Setup
---
Please ensure that all mentioned dependencies have been installed and set up.


There are 2 repos that need to be cloned and set up:


<h3>1. Admin Panel</h3>

Clone Repo from the below URL and execute the below commands:

```
git clone https://gitlab.com/giraffe-unicef/giraffe-ontology-editor-frontend
npm install 
npm start
```

To access, go to http://localhost:3000


<h3>3. Backend</h3>

1. Clone Repo `git clone https://gitlab.com/giraffe-unicef/giraffe-job-recommendation-ontology.git`
2. Import project in IDE (ecliple, IntelliJ IDEA )
3. Create database and import sql from `src\main\resources\schema.sql`
4. Update <i>application.properties</i>
5. Start application from IDE or follow the below steps:
    * Go to the root of the application where `build.gradle` is available 
    * Run execute the below command `gradle bootRun`
6. Go to `http://localhost:8081/v1/swagger-ui.html`